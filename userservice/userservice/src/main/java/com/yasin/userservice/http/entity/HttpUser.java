package com.yasin.userservice.http.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.yasin.userservice.entity.User;


@JsonInclude(value=Include.NON_NULL)
public class HttpUser {

	public long id;
	
	public String firstName;
	
	public String lastName;
		
	public int status;

	public HttpUser() {
		// TODO Auto-generated constructor stub
	}
	
	public HttpUser(User user) {
		this.id = user.getId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.status = user.getStatus();
	}
	
}
