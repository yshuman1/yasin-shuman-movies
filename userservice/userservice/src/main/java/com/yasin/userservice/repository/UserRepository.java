package com.yasin.userservice.repository;


import org.springframework.data.repository.CrudRepository;

import com.yasin.userservice.entity.impl.UserImpl;


public interface UserRepository extends CrudRepository<UserImpl, Long>{
	Iterable<UserImpl> findByFirstNameContainingAndLastNameContaining(String firstName, String lastName);

	Iterable<UserImpl> findByFirstNameContaining(String firstName);

	Iterable<UserImpl> findByLastNameContaining(String firstName);
}
