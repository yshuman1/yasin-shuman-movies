package com.yasin.userservice.service;

import com.yasin.userservice.entity.User;
import com.yasin.userservice.entity.impl.UserImpl;

public interface UserService {
	
	User addUser(User user);

	void updateUser(User user);

	User getUser(long userId);
	
	//User getUser(String firstName, String lastName);

	/**
	 * Search user by first or last name, partial searches also performed
	 * 
	 * @param firstName
	 * @param lastName
	 * @return Empty list is returned if none found
	 */
	Iterable<UserImpl> getUsers(String firstName, String lastName);

}
