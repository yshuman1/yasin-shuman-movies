package com.yasin.userservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import com.yasin.userservice.entity.User;
import com.yasin.userservice.entity.impl.UserImpl;
import com.yasin.userservice.repository.UserRepository;
import com.yasin.userservice.service.ServiceException;
import com.yasin.userservice.service.UserService;


@Service
public class UserServiceImpl implements UserService{

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User addUser(User user) {
		if(user==null){
			try {
				throw new ServiceException("user is null");
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		UserImpl impl = (UserImpl)user;
		return userRepository.save(impl);
	}

	@Override
	public void updateUser(User user) {
		addUser(user);
	}

	@Override
	public User getUser(long userId) {
		User found = userRepository.findOne(userId);
		if(found==null){
			try {
				throw new ServiceException( "User not found for userId = " + userId);
			} catch (ServiceException e) {
				//e.printStackTrace();
				logger.info("no user found for userId = " + userId);
			}
		}
		return found;
		// TODO hard coded
//		UserImpl user = new UserImpl(1);
//		user.setFirstName("Yasin");
//		user.setLastName("Shuman");
//		user.setStatus(1);
//		return user;
	}

//	@Override
//	public User getUser(String firstName, String lastName) {
//		// Hard coded
//		UserImpl user = new UserImpl(1);
//		user.setFirstName("Yasin");
//		user.setLastName("Shuman");
//		user.setStatus(1);
//		return user;
//	}

	@Override
	public Iterable<UserImpl> getUsers(String firstName, String lastName) {
		// hard coded
//		UserImpl user = new UserImpl(1);
//		user.setFirstName("Yasin");
//		user.setLastName("Shuman");
//		user.setStatus(1);
//		UserImpl user2 = new UserImpl(2);
//		user2.setFirstName("John");
//		user2.setLastName("Smith");
//		user2.setStatus(2);
//		List<User> list = new ArrayList<>();
//		list.add(user);
//		list.add(user2);
//		return list;
		if(StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName)){
			try {
				throw new ServiceException("Atleast 1 required");
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(!StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName)){
			return userRepository.findByFirstNameContaining(firstName);
		}
		if(StringUtils.isEmpty(firstName) && !StringUtils.isEmpty(lastName)){
			return userRepository.findByLastNameContaining(lastName);
		}		
		
		return userRepository.findByFirstNameContainingAndLastNameContaining(firstName, lastName);
		
	}

}
