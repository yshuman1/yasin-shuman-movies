package com.yasin.userservice.entity;

public interface User {
	long getId();	
	String getFirstName();
	String getLastName();
	int getStatus();
}
