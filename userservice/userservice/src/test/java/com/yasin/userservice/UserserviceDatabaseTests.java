package com.yasin.userservice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.yasin.userservice.entity.User;
import com.yasin.userservice.entity.impl.UserImpl;
import com.yasin.userservice.service.ServiceException;
import com.yasin.userservice.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserserviceDatabaseTests {

	@Autowired
	private UserService userService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Test
	public void contextLoads()  throws Exception{
		assertTrue("Working" , userService != null);
	}
	
	@Test
	public void addUserTest(){
		UserImpl u1 = new UserImpl();
		u1.setFirstName("new test"+new Random().nextInt(99999));
		u1.setLastName("lastName");
		u1.setStatus(1);
		User added = userService.addUser(u1);
		assertTrue("Working" , u1.getFirstName().equals(added.getFirstName())
				&& u1.getLastName().equals(added.getLastName())
						&& u1.getId() == added.getId() 
						&& u1.getStatus() == added.getStatus());
	}
	
	@Test
	public void addAndGetUser(){
		UserImpl newUser = new UserImpl();
		newUser.setFirstName("test"+new Random().nextInt(99999));
		newUser.setLastName("lastName");
		newUser.setStatus(1);
		
		User added = userService.addUser(newUser);
		logger.info("user added "+added);
		assertThat(0).isNotEqualTo(added.getId());//this should have been created so not zero anymore
		assertThat(newUser.getFirstName()).isEqualTo(added.getFirstName());
		assertThat(newUser.getLastName()).isEqualTo(added.getLastName());
		assertThat(newUser.getStatus() == added.getStatus());
		
		User found = userService.getUser(added.getId());
		assertThat(found.getId()).isEqualTo(added.getId());
		assertThat(found.getFirstName()).isEqualTo(added.getFirstName());
		assertThat(found.getLastName()).isEqualTo(added.getLastName());
		assertThat(found.getStatus() == added.getStatus());
	}
	

	@Test
	public void getNonExistantUser(){
		User found = null;
		try {
			found = userService.getUser(Long.MAX_VALUE);
		} catch (Exception e) {
			logger.info("user non existant ");
			} 
		
		assertThat(found == null);
	}
	
	/*@Test
	public void getUsersTest(){
		UserImpl u1 = new UserImpl(1);
		u1.setFirstName("Yasin");
		u1.setLastName("Shuman");
		u1.setStatus(1);
		
		UserImpl u3 = new UserImpl(2);
		u3.setFirstName("John");
		u3.setLastName("Smith");
		u3.setStatus(2);
		
		Iterable<UserImpl> list = userService.getUsers("Yasin", "Shamin");
		for (User u2 : list) {
			boolean match1 = u1.getFirstName().equals(u2.getFirstName())
					&& u1.getLastName().equals(u2.getLastName())
					&& u1.getId() == u2.getId() 
					&& u1.getStatus() == u2.getStatus();
			boolean match2 = u3.getFirstName().equals(u2.getFirstName())
					&& u3.getLastName().equals(u2.getLastName())
					&& u3.getId() == u2.getId() 
					&& u3.getStatus() == u2.getStatus();
			assertTrue("Working" , match1 || match2 );
		}	
	}*/
	


}
