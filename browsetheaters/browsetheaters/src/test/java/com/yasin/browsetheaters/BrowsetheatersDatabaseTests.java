package com.yasin.browsetheaters;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.yasin.browsetheaters.entity.Theater;
import com.yasin.browsetheaters.entity.impl.TheaterImpl;
import com.yasin.browsetheaters.service.TheaterService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BrowsetheatersDatabaseTests {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private TheaterService theaterService;

	@Test
	public void contextLoads() {
		assertTrue("Working" , theaterService != null);
	}

	@Test
	public void addTheaterTest(){
		TheaterImpl m1 = new TheaterImpl();
		m1.setName("new test"+new Random().nextInt(99999));
		m1.setAddress("address");


		Theater added = theaterService.addTheater(m1);
		assertTrue("Working" , (m1.getName()).equals(added.getName()) && (m1.getAddress()).equals(added.getAddress()) );
	}

	
	@Test
	public void addAndGetTheaterTest(){
		TheaterImpl m1 = new TheaterImpl();
		m1.setName("new test"+new Random().nextInt(99999));
		m1.setAddress("address");


		Theater added = theaterService.addTheater(m1);
		logger.info("Theater added "+added);
		assertThat(0).isNotEqualTo(added.getId());//this should have been created so not zero anymore
		assertThat(m1.getName()).isEqualTo(added.getName());
		assertThat(m1.getAddress() == added.getAddress());

		Theater found = theaterService.getTheater(added.getId());
		assertThat(found.getId() == added.getId());
		assertThat(found.getName()).isEqualTo(added.getName());
		assertThat((found.getAddress()).equals(added.getAddress()));

	}
	@Test
	public void getNonExistantTheater(){
		Theater found = null;
		try {
			found = theaterService.getTheater(Long.MAX_VALUE);
		} catch (Exception e) {
			logger.info("Theater non existant ");
		} 

		assertThat(found == null);
	}
	
	
}
