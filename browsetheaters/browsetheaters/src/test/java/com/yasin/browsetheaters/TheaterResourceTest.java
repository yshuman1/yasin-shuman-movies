package com.yasin.browsetheaters;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yasin.browsetheaters.entity.impl.TheaterImpl;
import com.yasin.browsetheaters.http.entity.HttpTheater;
import com.yasin.browsetheaters.repository.TheaterRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TheaterResourceTest {


	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@MockBean
	private TheaterRepository theaterRepository;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testGetTheaterNoParams() throws Exception {
		MvcResult mockResponse = mockMvc.perform(get("/theaters").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());

	}

	@Test
	public void testGetTheaterName() throws Exception {
		doReturn(Arrays.asList(new TheaterImpl())).when(theaterRepository).findByNameContaining("foo");
		
		MvcResult mockResponse = mockMvc.perform(get("/theaters").param("name","foo").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

		String expectedResponseBody = "[{\"id\":0,\"name\":null,\"address\":null}]";
		String mvcResponse = new String(mockResponse.getResponse().getContentAsByteArray());
		JSONAssert.assertEquals(expectedResponseBody, mvcResponse, true);
		
		verify(theaterRepository, times(1)).findByNameContaining("foo");
		verifyNoMoreInteractions(theaterRepository);
	}
	
	

	@Test
	public void testGetTheaterById() throws Exception {
		TheaterImpl mockTheater = new TheaterImpl();
		mockTheater.setName("mockFname");
		mockTheater.setAddress("address");
		doReturn(mockTheater).when(theaterRepository).findOne(123l);
		
		MvcResult mockResponse = mockMvc.perform(get("/theaters/123").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

		String expectedResponseBody = new String(Files.readAllBytes(Paths.get("src/test/resources/theaters-get.json")));
		String mvcResponse = new String(mockResponse.getResponse().getContentAsByteArray());
		JSONAssert.assertEquals(expectedResponseBody, mvcResponse, true);
		
		verify(theaterRepository, times(1)).findOne(123l);
		verifyNoMoreInteractions(theaterRepository);
	}

	
	@Test
	public void testCreateTheater() throws Exception {
		TheaterImpl mockTheater = new TheaterImpl();
		mockTheater.setName("foo");
		mockTheater.setAddress("address");


		doReturn(mockTheater).when(theaterRepository).save(any(TheaterImpl.class));
		
		MvcResult mockResponse = mockMvc.perform(post("/theaters").accept(MediaType.APPLICATION_JSON).content(getNewTheater())
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());

		String expectedResponseBody = new String(Files.readAllBytes(Paths.get("src/test/resources/theaters-create.json")));
		String mvcResponse = new String(mockResponse.getResponse().getContentAsByteArray());
		JSONAssert.assertEquals(expectedResponseBody, mvcResponse, true);
		
		verify(theaterRepository, times(1)).save(any(TheaterImpl.class));
		verifyNoMoreInteractions(theaterRepository);
	}

	private String getNewTheater() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		HttpTheater theater = new HttpTheater();
		theater.name = "foo";
		theater.address = "address";
		return mapper.writeValueAsString(theater);
	}
	
	
}
