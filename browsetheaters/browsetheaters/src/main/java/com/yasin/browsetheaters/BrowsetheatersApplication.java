package com.yasin.browsetheaters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrowsetheatersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrowsetheatersApplication.class, args);
	}
}
