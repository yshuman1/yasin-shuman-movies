package com.yasin.browsetheaters.service;

import com.yasin.browsetheaters.entity.Theater;
import com.yasin.browsetheaters.entity.impl.TheaterImpl;

public interface TheaterService {

	Theater addTheater(Theater theater);
	void updateTheater(Theater theater);
	Theater getTheater(Long id);
	Iterable<TheaterImpl> getTheaters(String name, String address);
}
