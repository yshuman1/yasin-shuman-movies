package com.yasin.browsetheaters.http.entity;

import com.yasin.browsetheaters.entity.Theater;

public class HttpTheater {

	public Long id;
	public String name;
	public String address;
	
	
	public HttpTheater() {
	}


	public HttpTheater(Theater theater) {
		id = theater.getId();
		name = theater.getName();
		address = theater.getAddress();
	}
	
	
}
