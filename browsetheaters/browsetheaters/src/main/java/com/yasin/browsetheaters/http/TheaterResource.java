package com.yasin.browsetheaters.http;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yasin.browsetheaters.entity.Theater;
import com.yasin.browsetheaters.entity.impl.TheaterImpl;
import com.yasin.browsetheaters.http.entity.HttpTheater;
import com.yasin.browsetheaters.service.TheaterService;

@RestController
@RequestMapping(value = "/theaters", produces = MediaType.APPLICATION_JSON_VALUE)
public class TheaterResource {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private TheaterService theaterService;
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HttpTheater> addTheater(@RequestBody HttpTheater newTheater){
		Theater theaterToAdd = convert(newTheater);
		logger.info("Create Theater:" + theaterToAdd);
		Theater theateradded = theaterService.addTheater(theaterToAdd);		
		return new ResponseEntity<>(new HttpTheater(theateradded),HttpStatus.CREATED);		
	}
	
	@RequestMapping(value = "/{theaterId}", method = RequestMethod.GET)
	public ResponseEntity<HttpTheater> getTheaterById(@PathVariable("theaterId") long theaterId) {
		logger.info("getting movie by id:" + theaterId);
		Theater movie = theaterService.getTheater(theaterId);
		return new ResponseEntity<>(new HttpTheater(movie), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<HttpTheater>> getTheaterSearch(@RequestParam(value="name", required=false) String name,
			@RequestParam(value="address", required=false) String address) {
		logger.info("Theater search name=" + name + " address=" + address);
		Iterable<TheaterImpl> found = theaterService.getTheaters(name, address);
		if (found == null){
			return new ResponseEntity<>(null, HttpStatus.CONFLICT);
		}
		List<HttpTheater> returnList = new ArrayList<>();
		for (Theater theater : found) {
			returnList.add(new HttpTheater(theater));
		}
		return new ResponseEntity<>(returnList, HttpStatus.OK);
	}
	
	
	private Theater convert(HttpTheater httpTheater){
		TheaterImpl theater = new TheaterImpl();
		//theater.setId(httpTheater.id);
		theater.setName(httpTheater.name);
		theater.setAddress(httpTheater.address);
		return theater;
	}
	
}
