package com.yasin.browsetheaters.repository;

import org.springframework.data.repository.CrudRepository;

import com.yasin.browsetheaters.entity.impl.*;

public interface TheaterRepository extends CrudRepository<TheaterImpl, Long>{

	Iterable<TheaterImpl> findByNameContainingAndAddressContaining(String name, String address);
	Iterable<TheaterImpl> findById(Long id);
	Iterable<TheaterImpl> findByNameContaining(String name);
	Iterable<TheaterImpl> findByAddressContaining(String address);
}
