package com.yasin.browsetheaters.entity;

public interface Theater {
	long getId();
	String getName();
	String getAddress();
}
