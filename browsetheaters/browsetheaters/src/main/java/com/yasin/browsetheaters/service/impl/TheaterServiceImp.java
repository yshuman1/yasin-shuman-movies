package com.yasin.browsetheaters.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.yasin.browsetheaters.entity.Theater;
import com.yasin.browsetheaters.entity.impl.TheaterImpl;
import com.yasin.browsetheaters.repository.TheaterRepository;
import com.yasin.browsetheaters.service.TheaterService;



@Service
public class TheaterServiceImp implements TheaterService {

private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private TheaterRepository theaterRepository;
	
	@Override
	public Theater addTheater(Theater theater) {
		TheaterImpl imp = (TheaterImpl) theater;
		return theaterRepository.save(imp);
	}

	@Override
	public void updateTheater(Theater theater) {
		if (theater.getId() < 1){
			//throw exception
		}
		addTheater(theater);

	}

	@Override
	public Theater getTheater(Long id) {
		Theater found = theaterRepository.findOne(id);
		if (found == null){
			//throw exception
		}
		return found;
	}

	@Override
	public Iterable<TheaterImpl> getTheaters(String name, String address) {
		if(StringUtils.isEmpty(name) && StringUtils.isEmpty(address)){
			//exception
			return null;
		}
		if(!StringUtils.isEmpty(name) && StringUtils.isEmpty(address)){
			return theaterRepository.findByNameContaining(name);
		}
		if(StringUtils.isEmpty(name) && !StringUtils.isEmpty(address)){
			return theaterRepository.findByAddressContaining(address);
		}		
		
		return theaterRepository.findByNameContainingAndAddressContaining(name, address);
		
	}

}
