package com.yasin.browsemovies;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yasin.browsemovies.entity.Movie;
import com.yasin.browsemovies.entity.impl.MovieImpl;
import com.yasin.browsemovies.http.entity.HttpMovie;
import com.yasin.browsemovies.repository.MovieRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovieResourceTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@MockBean
	private MovieRepository movieRepository;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	
	@Test
	public void testGetMovieNoParams() throws Exception {
		MvcResult mockResponse = mockMvc.perform(get("/movies").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());

	}
	
	
	@Test
	public void testGetMovieTitle() throws Exception {
		doReturn(Arrays.asList(new MovieImpl())).when(movieRepository).findByTitleContaining("foo");
		
		MvcResult mockResponse = mockMvc.perform(get("/movies").param("title","foo").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

		String expectedResponseBody = "[{\"id\":0,\"rating\":0.0,\"length\":0}]";
		String mvcResponse = new String(mockResponse.getResponse().getContentAsByteArray());
		JSONAssert.assertEquals(expectedResponseBody, mvcResponse, true);
		
		verify(movieRepository, times(1)).findByTitleContaining("foo");
		verifyNoMoreInteractions(movieRepository);
	}
	
	

	@Test
	public void testGetMovieById() throws Exception {
		MovieImpl mockMovie = new MovieImpl();
		mockMovie.setTitle("mockFname");
		doReturn(mockMovie).when(movieRepository).findOne(123l);
		
		MvcResult mockResponse = mockMvc.perform(get("/movies/123").accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

		String expectedResponseBody = new String(Files.readAllBytes(Paths.get("src/test/resources/movies-get.json")));
		String mvcResponse = new String(mockResponse.getResponse().getContentAsByteArray());
		JSONAssert.assertEquals(expectedResponseBody, mvcResponse, true);
		
		verify(movieRepository, times(1)).findOne(123l);
		verifyNoMoreInteractions(movieRepository);
	}

	
	@Test
	public void testCreateMovie() throws Exception {
		MovieImpl mockMovie = new MovieImpl();
		mockMovie.setTitle("foo");
		mockMovie.setRating(4.5);
		mockMovie.setLength(100);

		doReturn(mockMovie).when(movieRepository).save(any(MovieImpl.class));
		
		MvcResult mockResponse = mockMvc.perform(post("/movies").accept(MediaType.APPLICATION_JSON).content(getNewMovie())
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andReturn();
		assertThat(mockResponse.getResponse().getStatus()).isEqualTo(HttpStatus.CREATED.value());

		String expectedResponseBody = new String(Files.readAllBytes(Paths.get("src/test/resources/movies-create.json")));
		String mvcResponse = new String(mockResponse.getResponse().getContentAsByteArray());
		JSONAssert.assertEquals(expectedResponseBody, mvcResponse, true);
		
		verify(movieRepository, times(1)).save(any(MovieImpl.class));
		verifyNoMoreInteractions(movieRepository);
	}

	private String getNewMovie() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		HttpMovie movie = new HttpMovie();
		movie.title = "foo";
		movie.length = 100;
		movie.rating = 4.5;
		return mapper.writeValueAsString(movie);
	}
	
}
