package com.yasin.browsemovies;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.yasin.browsemovies.entity.Movie;
import com.yasin.browsemovies.entity.impl.MovieImpl;
import com.yasin.browsemovies.service.MovieService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BrowsemoviesDatabaseTests {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MovieService movieService;

	@Test
	public void contextLoads()  throws Exception{
		assertTrue("Working" , movieService != null);
	}

	@Test
	public void addMovieTest(){
		MovieImpl m1 = new MovieImpl();
		m1.setTitle("new test"+new Random().nextInt(99999));
		m1.setLength(220);
		m1.setRating(4.5);

		Movie added = movieService.addMovie(m1);
		assertTrue("Working" , m1.getTitle().equals(added.getTitle()) && m1.getLength() == added.getLength() && m1.getRating() == added.getRating());
	}

	@Test
	public void addAndGetMovieTest(){
		MovieImpl m1 = new MovieImpl();
		m1.setTitle("new test"+new Random().nextInt(99999));
		m1.setLength(220);
		m1.setRating(4.5);

		Movie added = movieService.addMovie(m1);
		logger.info("Movie added "+added);
		assertThat(0).isNotEqualTo(added.getId());//this should have been created so not zero anymore
		assertThat(m1.getTitle()).isEqualTo(added.getTitle());
		assertThat(m1.getLength() == added.getLength());
		assertThat(m1.getRating() == added.getRating());

		Movie found = movieService.getMovie(added.getId());
		assertThat(found.getId() == added.getId());
		assertThat(found.getTitle()).isEqualTo(added.getTitle());
		assertThat(found.getLength() == added.getLength());
		assertThat(found.getRating() == added.getRating());

	}
	@Test
	public void getNonExistantMovie(){
		Movie found = null;
		try {
			found = movieService.getMovie(Long.MAX_VALUE);
		} catch (Exception e) {
			logger.info("Movie non existant ");
		} 

		assertThat(found == null);
	}
	/*
	@Test
	public void getMovieTest(){
		MovieImpl m1 = new MovieImpl(1);
		m1.setTitle("Wonder Woman");
		m1.setLength(220);
		m1.setRating(4.5);
		Movie m2 = movieService.getMovie(1);
		assertTrue("Working" , m1.getTitle().equals(m2.getTitle()) && m1.getLength() == m2.getLength() && m1.getRating() == m2.getRating());

	}

	@Test
	public void getMoviesTest(){

		//hard coded movies
				MovieImpl m1 = new MovieImpl(1);
				m1.setTitle("Wonder Woman");
				m1.setLength(220);
				m1.setRating(4.5);
				MovieImpl m3 = new MovieImpl(2);
				m3.setTitle("Spider-Man: Homecoming");
				m3.setLength(250);
				m3.setRating(4.1);

				Iterable<MovieImpl> moives = movieService.getMovies("Wonder Woman");
				for (Movie m2 : moives) {
					boolean match1 = m1.getTitle().equals(m2.getTitle()) && m1.getLength() == m2.getLength() && m1.getRating() == m2.getRating();
					boolean match2 = m3.getTitle().equals(m2.getTitle()) && m3.getLength() == m2.getLength() && m3.getRating() == m2.getRating() ;
					assertTrue("Working" , match1 || match2 );

				}

	}
	 */
}
