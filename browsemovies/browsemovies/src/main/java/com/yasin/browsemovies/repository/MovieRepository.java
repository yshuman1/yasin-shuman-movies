package com.yasin.browsemovies.repository;

import com.yasin.browsemovies.entity.impl.MovieImpl;
import org.springframework.data.repository.CrudRepository;

public interface MovieRepository extends CrudRepository<MovieImpl, Long>{
	
	Iterable<MovieImpl> findByTitleContaining(String title);
	
	Iterable<MovieImpl> findById(Long id);
	
	//Iterable<MovieImpl> findByTitleContaining(String title);

}
