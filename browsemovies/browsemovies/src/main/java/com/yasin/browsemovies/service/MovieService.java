package com.yasin.browsemovies.service;

import com.yasin.browsemovies.entity.Movie;
import com.yasin.browsemovies.entity.impl.MovieImpl;

public interface MovieService {
	
	Movie addMovie(Movie movie);
	
	void updateMovie(Movie movie);
	
	Movie getMovie(long id);
	
	/**
	 * Search movie by title  partial searches also performed
	 * 
	 * @param title movie title/name 
	 * @return Empty list is returned if none found
	 */
	Iterable<MovieImpl> getMovies(String title);
	
	
}
