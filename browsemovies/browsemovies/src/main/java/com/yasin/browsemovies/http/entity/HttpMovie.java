package com.yasin.browsemovies.http.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.yasin.browsemovies.entity.Movie;

@JsonInclude(value=Include.NON_NULL)
public class HttpMovie {

	public long id;
	
	public String title;
	
	public double rating;
	
	public int length;
	
	
	
	public HttpMovie() {
		
	}



	public HttpMovie(Movie movie){
		this.id = movie.getId();
		this.title = movie.getTitle();
		this.rating = movie.getRating();
		this.length = movie.getLength();
	}
	
}
