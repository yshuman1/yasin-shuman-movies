package com.yasin.browsemovies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrowsemoviesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrowsemoviesApplication.class, args);
	}
}
