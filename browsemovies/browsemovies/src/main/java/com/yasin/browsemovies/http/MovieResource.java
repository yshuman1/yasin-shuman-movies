package com.yasin.browsemovies.http;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yasin.browsemovies.entity.Movie;
import com.yasin.browsemovies.entity.impl.MovieImpl;
import com.yasin.browsemovies.http.entity.HttpMovie;
import com.yasin.browsemovies.service.MovieService;

@RestController
@RequestMapping(value = "/movies", produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieResource {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieService movieService;
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HttpMovie> addMovie(@RequestBody HttpMovie newMovie){
		Movie movieToAdd = convert(newMovie);
		logger.info("Create Movie:" + movieToAdd);
		Movie movieadded = movieService.addMovie(movieToAdd);		
		return new ResponseEntity<>(new HttpMovie(movieadded),HttpStatus.CREATED);		
	}
	
	@RequestMapping(value = "/{movieId}", method = RequestMethod.GET)
	public ResponseEntity<HttpMovie> getMovieById(@PathVariable("movieId") long movieId) {
		logger.info("getting movie by id:" + movieId);
		Movie movie = movieService.getMovie(movieId);
		return new ResponseEntity<>(new HttpMovie(movie), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<HttpMovie>> getMovieSearch(@RequestParam(value="title", required=false) String title) {
		logger.info("movie search title=" + title);
		Iterable<MovieImpl> found = movieService.getMovies(title);
		if (found == null){
			return new ResponseEntity<>(null, HttpStatus.CONFLICT);
		}
		List<HttpMovie> returnList = new ArrayList<>();
		for (Movie movie : found) {
			returnList.add(new HttpMovie(movie));
		}
		return new ResponseEntity<>(returnList, HttpStatus.OK);
	}
	
	
	private Movie convert(HttpMovie httpMovie){
		MovieImpl movie = new MovieImpl();
		movie.setTitle(httpMovie.title);
		movie.setLength(httpMovie.length);
		movie.setLength(httpMovie.length);
		return movie;
	}

}
