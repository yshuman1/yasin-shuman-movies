package com.yasin.browsemovies.entity.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.yasin.browsemovies.entity.Movie;


@Entity
@Table(name = "movie")
public class MovieImpl implements Movie{
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "rating")
	private double rating;
	
	@Column(name = "length")
	private int length;
	
	public MovieImpl() {

	}
	
	public MovieImpl(long id) {
		this.id = id;
	}

	@Override
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	@Override
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return "MovieImpl [id=" + id + ", title=" + title + ", rating=" + rating + ", length=" + length + "]";
	}
	
	

}
