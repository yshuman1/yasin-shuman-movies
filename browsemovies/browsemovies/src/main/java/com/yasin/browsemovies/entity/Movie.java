package com.yasin.browsemovies.entity;

public interface Movie {
	long getId();
	String getTitle();
	double getRating();
	int getLength();
}
