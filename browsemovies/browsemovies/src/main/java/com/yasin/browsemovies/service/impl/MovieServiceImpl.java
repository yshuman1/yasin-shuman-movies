package com.yasin.browsemovies.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.yasin.browsemovies.entity.Movie;
import com.yasin.browsemovies.entity.impl.MovieImpl;
import com.yasin.browsemovies.repository.MovieRepository;
import com.yasin.browsemovies.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService{

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MovieRepository movieRepository;
	
	@Override
	public Movie addMovie(Movie movie) {
		
		MovieImpl impl = (MovieImpl) movie;
		return movieRepository.save(impl);
	}

	@Override
	public void updateMovie(Movie movie) {
		if (movie.getId() < 1){
			//throw exception - id required
		}
		addMovie(movie);

	}

	@Override
	public Movie getMovie(long id) {
		//hard coded movies
//		MovieImpl m1 = new MovieImpl(id);
//		m1.setTitle("Wonder Woman");
//		m1.setLength(220);
//		m1.setRating(4.5);
//		return m1;
		
		Movie found = movieRepository.findOne(id);
		if (found == null){
			//throw exception
		}
		return found;
	}

	@Override
	public Iterable<MovieImpl> getMovies(String title) {
//		List<Movie> list = new ArrayList<>();
//
//		//hard coded movies
//		MovieImpl m1 = new MovieImpl(1);
//		m1.setTitle("Wonder Woman");
//		m1.setLength(220);
//		m1.setRating(4.5);
//		MovieImpl m2 = new MovieImpl(2);
//		m2.setTitle("Spider-Man: Homecoming");
//		m2.setLength(250);
//		m2.setRating(4.1);
//
//		//
//		list.add(m1);
//		list.add(m2);
//		return list;
		
		if(StringUtils.isEmpty(title) ){
			//throw exception
		}
		return movieRepository.findByTitleContaining(title);
	}

}
